package com.stolbunov.roman.homework_16.bl.arenas;

import com.stolbunov.roman.homework_16.data.fighters.ArenaFighter;

public class DuelArenaFactory {
    public static BattleArena create(ArenaFighter fighter1, ArenaFighter fighter2) {
        return new DuelArena(null, fighter1, fighter2, 5);
    }
}
