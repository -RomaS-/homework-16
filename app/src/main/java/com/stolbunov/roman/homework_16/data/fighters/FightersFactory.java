package com.stolbunov.roman.homework_16.data.fighters;

import com.stolbunov.roman.homework_16.data.fighters.dragonRiders.DragonRider;
import com.stolbunov.roman.homework_16.data.fighters.dragons.Dragon;
import com.stolbunov.roman.homework_16.data.fighters.knights.BlackKnight;
import com.stolbunov.roman.homework_16.data.fighters.knights.HollyKnight;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class FightersFactory {
    public static final int DEFAULT_VALUE_HEALTH = 300;
    public static final int DEFAULT_VALUE_DAMAGE = 30;
    private static int counterNames = 0;

    private static  HashMap<FighterType, String> urlDataBase = new HashMap<>();
    private static List<String> listNames = new ArrayList<>();
    private static HashMap<FighterType, String> classDescriptions = new HashMap<>();

    static {
        additionImageURI();
        additionRandomName();

        additionClassDescription();
    }

    private static void additionClassDescription() {
        classDescriptions.put(
                FighterType.DRAGON,
                "Dragons have giant fangs and claws, and are also skillful lords of the elements." +
                        " As a rule, dragons possess not one, but two kinds of elements such as " +
                        "water, fire, earth, air.");

        classDescriptions.put(
                FighterType.DRAGON_RIDER,
                "Another baby was found by the great Dragon Warrior and raised him as his own son," +
                        " teaching him both language and tactics of fighting. Although the " +
                        " Dragon Rider does not have a strong armor, it compensates him with excellent" +
                        " dexterity and discretion, and if the Dragon Rider on the way meets the Dragon," +
                        " then they form a strong alliance rom which all enemies tremble.");

        classDescriptions.put(
                FighterType.HOLLY_KNIGHT,
                "Holly Knight since childhood was brought up at the church." +
                        " Love of neighbor and compassion in his blood, as well as a" +
                        " strong desire to ruthlessly punish any creature of darkness." +
                        " Being a good student, he mastered the skill of close combat and healing.");

        classDescriptions.put(
                FighterType.BLACK_KNIGHT,
                "The Black Knight in the past was a great warrior of light. But one day his family" +
                        " was mercilessly killed and he swore that he would avenge themselves for" +
                        " them at any cost. This desire long consumed him from the inside until it" +
                        " completely absorbed it. This situation was used by the local Vampire Lord," +
                        " making him one of them and also his personal student. After many years he" +
                        " became one of the strongest warriors of darkness and also the" +
                        " commander-in-chief of the entire army of the Vampire Lord.");
    }

    private static void additionRandomName() {
        listNames.add("Arthur");
        listNames.add("Leonard");
        listNames.add("Sheldon");
        listNames.add("Jack");
        listNames.add("Carl");
        listNames.add("Harry");
        listNames.add("Gregory");
        listNames.add("Richard");
    }

    private static void additionImageURI() {
        urlDataBase.put(FighterType.DRAGON, "https://avatarko.ru/img/kartinka/1/fantastika_drakon.jpg");
        urlDataBase.put(FighterType.DRAGON_RIDER, "http://static.tvtropes.org/pmwiki/pub/images/FireEmblemHeath_2008.jpg");
        urlDataBase.put(FighterType.KNIGHT, "https://thumbs.dreamstime.com/b/%D1%81%D0%BC%D0%B5%D1%88%D0%BD%D0%BE%D0%B9-%D1%80%D1%8B%D1%86%D0%B0%D1%80%D1%8C-67339345.jpg");
        urlDataBase.put(FighterType.HOLLY_KNIGHT, "http://cosmo.net.ru/images/news/warrior.png");
        urlDataBase.put(FighterType.BLACK_KNIGHT, "https://mbtskoudsalg.com/images/black-knight-png-5.png");
    }

    static class UnknownFighterException extends RuntimeException {
    }

    public static ArenaFighter generateFighter(FighterType fighterType) {
        return generateFighter(fighterType, generateName());
    }

    public static ArenaFighter generateFighter(FighterType fighterType, String name) {
        switch (fighterType) {
            case DRAGON:
                return new Dragon(name,
                        generateValue(DEFAULT_VALUE_HEALTH),
                        generateValue(DEFAULT_VALUE_DAMAGE),
                        generatePercentValue(),
                        0,
                        urlDataBase.get(fighterType));
            case DRAGON_RIDER:
                return new DragonRider(name,
                        generateValue(DEFAULT_VALUE_HEALTH),
                        generateValue(DEFAULT_VALUE_DAMAGE),
                        generatePercentValue(),
                        urlDataBase.get(fighterType));
            case HOLLY_KNIGHT:
                return new HollyKnight(name,
                        generateValue(DEFAULT_VALUE_HEALTH),
                        generateValue(DEFAULT_VALUE_DAMAGE),
                        generatePercentValue(),
                        generatePercentValue(),
                        generatePercentValue(),
                        urlDataBase.get(fighterType));
            case BLACK_KNIGHT:
                return new BlackKnight(name,
                        generateValue(DEFAULT_VALUE_HEALTH),
                        generateValue(DEFAULT_VALUE_DAMAGE),
                        generatePercentValue(),
                        generatePercentValue(),
                        urlDataBase.get(fighterType));

//            case KNIGHT:
//                return new Knight(name, generateValue(DEFAULT_VALUE_HEALTH),
//                        generateValue(DEFAULT_VALUE_DAMAGE),
//                        generatePercentValue(),
//                        generatePercentValue(),
//                        urlDataBase.get(fighterType));

        }
        throw new UnknownFighterException();
    }

    public static float generateValue(int max) {
        Random random = new Random();
        return Math.abs(random.nextInt(max));

    }

    public static float generatePercentValue() {
        Random random = new Random();
        return (float) Math.abs(random.nextGaussian());
    }

    public static String generateName() {
        String name;
        if (counterNames < listNames.size()) {
            name = listNames.get(counterNames);
        } else {
            counterNames = 0;
            name = listNames.get(counterNames);
        }
        counterNames++;
        return name;
    }

    public static String getClassDescriptions(FighterType type) {
        return classDescriptions.get(type);
    }

    public static String getFighterImageURL(FighterType type) {
        return urlDataBase.get(type);
    }
}
