package com.stolbunov.roman.homework_16.data.fighters;

public interface PostAttackAction {
    void postAttackAction(float damageTaken, float damageGotten);
}
