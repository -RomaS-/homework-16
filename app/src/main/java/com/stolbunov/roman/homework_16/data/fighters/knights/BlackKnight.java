package com.stolbunov.roman.homework_16.data.fighters.knights;

import android.os.Parcel;

import com.stolbunov.roman.homework_16.data.fighters.FighterType;

public class BlackKnight extends Knight{

    public BlackKnight(Parcel in) {
        super(in);
    }

    public BlackKnight(String name, float health, float damage, float armor, float shield, String imageUrl) {
        super(name, health, damage, armor, shield, imageUrl);
    }

    private void recovery(float damageTaken) {
        if (this.isAlfie()) {
            float recovery = damageTaken / 2;
            this.heal(recovery);
            System.out.println(this.getName() + " recovery " + recovery);
        }
    }

    @Override
    protected int getType() {
        return FighterType.BLACK_KNIGHT.getType();
    }

    @Override
    public FighterType getClassFighter() {
        return FighterType.BLACK_KNIGHT;
    }
}
