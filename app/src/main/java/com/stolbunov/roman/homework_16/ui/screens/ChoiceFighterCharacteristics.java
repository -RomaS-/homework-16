package com.stolbunov.roman.homework_16.ui.screens;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.stolbunov.roman.homework_16.R;
import com.stolbunov.roman.homework_16.data.fighters.ArenaFighter;
import com.stolbunov.roman.homework_16.data.fighters.FighterType;
import com.stolbunov.roman.homework_16.data.fighters.FightersFactory;

public class ChoiceFighterCharacteristics extends AppCompatActivity {
    private ImageView fighterImage;
    private TextView fighterName;
    private TextView fighterClass;
    private TextView fighterDescription;
    private TextView fighterHP;
    private TextView fighterArmor;
    private TextView fighterDamage;
    private Button fightStart;
    private Button fighterReplace;

    String nameType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choice_fighter_characteristics);

        Intent intent = getIntent();
        nameType = intent.getStringExtra(ChoiceFighter.SELECTED_FIGHTER);

        initViews();
        fillingView();
    }

    private void fillingView() {
        MainActivity.loadingImage(this,
                FightersFactory.getFighterImageURL(FighterType.valueOf(nameType)),
                fighterImage);
        fighterDescription.setText(FightersFactory.getClassDescriptions(FighterType.valueOf(nameType)));
        replaceView();
    }

    private void replaceView() {
        fighterName.setText(FightersFactory.generateName());
        fighterClass.setText(nameType);
        fighterHP.setText(String.valueOf(FightersFactory.generateValue(FightersFactory.DEFAULT_VALUE_HEALTH)));
        fighterDamage.setText(String.valueOf(FightersFactory.generateValue(FightersFactory.DEFAULT_VALUE_DAMAGE)));
        fighterArmor.setText(String.valueOf(FightersFactory.generatePercentValue()));
    }

    private void initViews() {
        fighterImage = findViewById(R.id.iv_image_fighter);
        fighterName = findViewById(R.id.tv_value_name);
        fighterClass = findViewById(R.id.tv_class_fighter);
        fighterDescription = findViewById(R.id.tv_description);
        fighterHP = findViewById(R.id.value_hp);
        fighterArmor = findViewById(R.id.value_armor);
        fighterDamage = findViewById(R.id.value_damage);
        fightStart = findViewById(R.id.bt_start_fight);
        fighterReplace = findViewById(R.id.bt_replace_ch);

        fightStart = findViewById(R.id.bt_start_fight);
        fighterReplace = findViewById(R.id.bt_replace_ch);

        fighterReplace.setOnClickListener(this::onClick);
        fightStart.setOnClickListener(this::onClick);
    }

    private void returnChosenFighter() {
        Intent intent = new Intent(this, ChoiceFighter.class);
        intent.putExtra(ChoiceFighter.SELECTED_FIGHTER, createChosenFighter());
        setResult(RESULT_OK, intent);
        finish();
    }

    private ArenaFighter createChosenFighter() {
        return FightersFactory.generateFighter(
                FighterType.valueOf(nameType),
                fighterName.getText().toString());
    }

    private void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_replace_ch:
                replaceView();
                break;
            case R.id.bt_start_fight:
                returnChosenFighter();
                break;
        }
    }
}
