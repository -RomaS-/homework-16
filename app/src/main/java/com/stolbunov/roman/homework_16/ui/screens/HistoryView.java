package com.stolbunov.roman.homework_16.ui.screens;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.stolbunov.roman.homework_16.R;

public class HistoryView {
    private LayoutInflater inflater;
    private ImageView battleImageFighter1;
    private ImageView battleImageFighter2;
    private ImageView battleImageVS;
    private TextView battleNameFighter1;
    private TextView battleNameFighter2;
    private TextView battleHPFighter1;
    private TextView battleHPFighter2;
    private TextView battleResult;
    private View battleView;

    public HistoryView(LayoutInflater inflater, ViewGroup parent) {
        this.inflater = inflater;
        battleView = inflater.inflate(R.layout.show_battle_fighters, parent, false);

        battleImageFighter1 = battleView.findViewById(R.id.iv_battle_fighter1);
        battleImageFighter2 = battleView.findViewById(R.id.iv_battle_fighter2);
        battleImageVS = battleView.findViewById(R.id.iv_battle_vs);
        battleNameFighter1 = battleView.findViewById(R.id.tv_name_fighter1);
        battleNameFighter2 = battleView.findViewById(R.id.tv_name_fighter2);
        battleHPFighter1 = battleView.findViewById(R.id.tv_hp_fighter1);
        battleHPFighter2 = battleView.findViewById(R.id.tv_hp_fighter2);
        battleResult = battleView.findViewById(R.id.tv_battle_result);
    }

    public void setBattleImageFighter1(String uriImage) {
        MainActivity.loadingImage(inflater.getContext(), uriImage, battleImageFighter1);
    }

    public void setBattleImageFighter2(String uriImage) {
        MainActivity.loadingImage(inflater.getContext(), uriImage, battleImageFighter2);
    }

    public void setBattleImageVS(String uriImage) {
        MainActivity.loadingImage(inflater.getContext(), uriImage, battleImageVS);
    }

    public void setBattleNameFighter1(String nameFighter) {
        battleNameFighter1.setText(nameFighter);
    }

    public void setBattleNameFighter2(String nameFighter) {
        battleNameFighter2.setText(nameFighter);
    }

    public void setBattleHPFighter1(String fighterHP) {
        battleHPFighter1.setText(fighterHP);
    }

    public void setBattleHPFighter2(String fighterHP) {
        battleHPFighter2.setText(fighterHP);
    }

    public void setBattleResult(String battleResult) {
        this.battleResult.setText(battleResult);
    }

    public View getView() {
        return battleView;
    }
}
