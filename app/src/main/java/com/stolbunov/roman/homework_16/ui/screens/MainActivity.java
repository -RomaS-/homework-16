package com.stolbunov.roman.homework_16.ui.screens;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.stolbunov.roman.homework_16.R;
import com.stolbunov.roman.homework_16.bl.arenas.BattleArena;
import com.stolbunov.roman.homework_16.bl.arenas.DuelArenaFactory;
import com.stolbunov.roman.homework_16.data.fighters.ArenaFighter;
import com.stolbunov.roman.homework_16.data.fighters.FighterType;
import com.stolbunov.roman.homework_16.data.fighters.FightersFactory;

public class MainActivity extends AppCompatActivity {
    public static final int RC_CHOICE_FIGHTER = 159;
    private static final String CURRENT_FIGHTER_KEY = "CURRENT_FIGHTER_KEY";
    private static final String URI_VS_IMAGE =
            "http://static1.comicvine.com/uploads/original/11112/111129141/5440487-1122329314-52705.png";

    private float startHPFighter;
    private float startHPEnemy;

    private ImageView profileImage;

    private TextView profileName;
    private TextView profileDescription;

    private TextView currentHp;
    private TextView currentArmor;
    private TextView currentDamage;

    private TextView startNewChange;

    private LinearLayout battleHistoryParent;
    private HistoryView historyView;

    private ArenaFighter currentFighter;
    private ArenaFighter currentEnemy;
    private ArenaFighter currentWinner;

    private BattleArena arena;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        initFighter(savedInstanceState);
        setDataOnUi();

    }

    private void setDataOnUi() {
        loadingImage(this, currentFighter, profileImage);

        profileName.setText(currentFighter.getName());
        profileDescription.setText(FightersFactory.getClassDescriptions(currentFighter.getClassFighter()));

        currentHp.setText(String.valueOf(currentFighter.getHealth()));
        currentDamage.setText(String.valueOf(currentFighter.getDamage()));
        currentArmor.setText(String.valueOf(currentFighter.getArmor()));
    }

    private void initFighter(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            currentFighter = FightersFactory.generateFighter(FighterType.DRAGON_RIDER, "Ikking");
        } else {
            currentFighter = savedInstanceState.getParcelable(CURRENT_FIGHTER_KEY);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(CURRENT_FIGHTER_KEY, currentFighter);
    }

    private void choiceEnemy(View view) {
        Intent intent = new Intent(this, ChoiceFighter.class);
        startActivityForResult(intent, RC_CHOICE_FIGHTER);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null && resultCode == RESULT_OK) {
            switch (requestCode) {
                case RC_CHOICE_FIGHTER:
                    startNewFight(data.getParcelableExtra(ChoiceFighter.SELECTED_FIGHTER));
            }
        }
    }

    private void startNewFight(ArenaFighter enemy) {
        if (currentFighter.getHealth() > 0) {
            currentEnemy = enemy;
            startHPFighter = currentFighter.getHealth();
            startHPEnemy = enemy.getHealth();
            arena = DuelArenaFactory.create(currentFighter, enemy);
            arena.startBattle();
            currentWinner = arena.getWinner();
            addFightResultOnLayout();
        } else {
            Toast.makeText(this, R.string.fighter_died, Toast.LENGTH_SHORT).show();
        }
    }

    private void addFightResultOnLayout() {
        initViewForInflate();
        fillingView();
        battleHistoryParent.addView(historyView.getView());
    }

    private void fillingView() {
        historyView.setBattleImageFighter1(currentFighter.getImageUrl());
        historyView.setBattleImageFighter2(currentEnemy.getImageUrl());
        historyView.setBattleImageVS(URI_VS_IMAGE);
        historyView.setBattleHPFighter1(String.valueOf(startHPFighter));
        historyView.setBattleHPFighter2(String.valueOf(startHPEnemy));
        historyView.setBattleNameFighter1(currentFighter.getName());
        historyView.setBattleNameFighter2(currentEnemy.getName());
        historyView.setBattleResult(String.format(
                getResources().getString(R.string.battle_result),
                currentWinner.getName(),
                currentWinner.getTotalDamage(),
                currentWinner.getHealth()));
    }

    private void initViews() {
        profileImage = findViewById(R.id.fighter_icon);

        profileName = findViewById(R.id.fighter_name);
        profileDescription = findViewById(R.id.fighter_description);

        currentHp = findViewById(R.id.value_hp);
        currentArmor = findViewById(R.id.value_armor);
        currentDamage = findViewById(R.id.value_damage);

        startNewChange = findViewById(R.id.btn_start_new_chalange);
        startNewChange.setOnClickListener(this::choiceEnemy);

        battleHistoryParent = findViewById(R.id.battle_history);
    }

    private void loadingImage(Context context, ArenaFighter fighter, ImageView view) {
        loadingImage(context, fighter.getImageUrl(), view);
    }

    public static void loadingImage(Context context, String uri, ImageView view) {
        Glide.with(context)
                .load(uri)
                .into(view);
    }

    private void initViewForInflate() {
        LayoutInflater inflater = getLayoutInflater();
        historyView = new HistoryView(inflater, battleHistoryParent);
    }


}
